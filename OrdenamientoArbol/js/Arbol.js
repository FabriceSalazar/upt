class Arbol{
    constructor(){
        this.NodoPadre = this.CrearPadre();
        this.Nodos = [];
    }

    CrearPadre(padre, nivel, valor, posicion){
        var nodo = new Nodo(padre, nivel, valor, posicion);
        //this.Nodos.push();
        return nodo;
    }
    
    CrearNodo(padre = null, nivel = null, valor, posicion = null){
        let nodo = new Nodo(padre, nivel, valor, posicion);

        if(this.Nodos.length == 0){
            nodo.padre = this.NodoPadre;
            nodo.nivel = this.NodoPadre.nivel + 1;

            if(nodo.valor < nodo.NodoPadre.valor){
                nodo.posicion = "Derecho";
            } else{
                nodo.posicion = "Izquierdo";
            }

            this.Nodos.push(nodo);
        }
        return nodo;
    }
}
