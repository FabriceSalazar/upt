class Nodo{
    constructor(padre, nivel, valor, posicion){
        this.valor = valor;
        this.padre = padre;
        this.posicion =  posicion;
        
        nivel == null ? this.nivel = 0 : this.nivel = nivel + 1;
    }
}