class Nodo{
    constructor(padre, tipo, nivel, valor){
        this.valor = valor;
        this.padre = padre;
        this.tipo = tipo;

        nivel == null ? this.nivel = 0 : this.nivel = nivel + 1;
    }
}