class Arbol{
    constructor(){
        this.NodoPadre = this.CrearPadre();
        this.Familia = {
            "Alfonso" : {
                "Nombre" : "Alfonso Fabrice",
                "Apellidos" : "Salazar Campos",
                "Gustos" : {
                    "Musica" : ["Foreigner", "Muse", "avenged Sevenfold"],
                    "Videojuegos" : ["Halo", "Hollow Knigth", "Doom"],
                    "PasaTiempos" : "Bicicleta"
                }
            },
        
            "Ricardo" : {
                "Nombre" : "Ricardo Jesus",
                "Apellidos" : "Chavez Martinez",
                "Gustos" : {
                    "Musica" : ["Muse", "el cuarteto de NOS", "Gorillaz"],
                    "Pasatiempos" : ["Dormir", "Dibujar"],
                    "Comidas" : ["Maruchan", "Pizza"]
                }
        
            },
        
            "Brandon" : {
                "Nombre" : "Jose Brandon",
                "Apellidos" : "Salazar Campos",
                "Gustos" : {
                    "Musica" : ["Chalino", "El Fantasma", "Los cadetes de linares", "Valentin elizalde"],
                    "Pasatiempos" : ["Ver vidios", "Manuela", "Ciclismo"],
                    "Comidas" : ["Maruchan", "Pizza"]
                }
        
            }
        
        };
    }

    CrearPadre(padre = null, tipo = "central", nivel = null, valor = 15){
        var nodo = new Nodo(padre, tipo, nivel, valor);
        return nodo;
    }
    
    CrearNodo(padre, tipo, nivel, valor){
        var nodo = new Nodo(padre, tipo, nivel, valor);
        return nodo;
    }
}
