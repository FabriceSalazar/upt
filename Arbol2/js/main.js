var arbol = new Arbol();
var tp = [] , z , camino = '' , suma = 0;

//Llenado del arbol
arbol.NodoPadre.Ni = arbol.CrearNodo(arbol.NodoPadre, "Izquierdo", arbol.NodoPadre.nivel, 9);
arbol.NodoPadre.Nd = arbol.CrearNodo(arbol.NodoPadre, "Derecho", arbol.NodoPadre.nivel, 20);

arbol.NodoPadre.Ni.Ni = arbol.CrearNodo(arbol.NodoPadre.Ni, "Izquierdo", arbol.NodoPadre.Ni.nivel, 6);
arbol.NodoPadre.Ni.Nd = arbol.CrearNodo(arbol.NodoPadre.Ni, "Derecho", arbol.NodoPadre.Ni.nivel, 14);

arbol.NodoPadre.Ni.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Ni.Nd, "Izquierdo", arbol.NodoPadre.Ni.Nd.nivel, 13);

arbol.NodoPadre.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd, "Izquierdo", arbol.NodoPadre.Nd.nivel, 17);
arbol.NodoPadre.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd, "Derecho", arbol.NodoPadre.Nd.nivel, 64);

arbol.NodoPadre.Nd.Nd.Ni = arbol.CrearNodo(arbol.NodoPadre.Nd.Nd, "Izquierdo", arbol.NodoPadre.Nd.Nd.nivel, 26);
arbol.NodoPadre.Nd.Nd.Nd = arbol.CrearNodo(arbol.NodoPadre.Nd.Nd, "Derecho", arbol.NodoPadre.Nd.Nd.nivel, 72);

//Imprime el arbol
console.log(arbol);

//Busqueda por nivel
var nivel = prompt("Ingrese un nivel:");
BusquedaNivel(arbol.NodoPadre, nivel);
console.log("Elementos en el nivel " + nivel + " : " + tp);

//Busqueda por elemento
var nel = prompt("Ingrese un valor a buscar:");
console.log("El elemento " + nel + " se encuentra en el nodo: ");
var nodox = BusquedaElemento(arbol.NodoPadre, nel);
console.log(nodox);

//Camino nodo
var cam = CaminoNodo(nodox, nel);
console.log(cam);

//Suma de elementos
var sum = CostoNodo(nodox, nel);
console.log(sum);

//Funciones

function BusquedaNivel(Nodo, nivel){
    
    if(Nodo.nivel == nivel)
        tp.push(Nodo.valor);

    if(Nodo.hasOwnProperty('Ni'))
        BusquedaNivel(Nodo.Ni, nivel);
    
    if(Nodo.hasOwnProperty('Nd'))
        BusquedaNivel(Nodo.Nd, nivel);

}

function BusquedaElemento(Nodo, nel){

    if(Nodo.hasOwnProperty('Ni'))
        BusquedaElemento(Nodo.Ni, nel);
    
    if(Nodo.hasOwnProperty('Nd'))
        BusquedaElemento(Nodo.Nd, nel);

    if(Nodo.valor == nel){
        z = Nodo;
    }

    return z;
}

function CaminoNodo(Nodo, nel){

    if(Nodo.padre != null){
        camino = camino + ' ' + Nodo.padre.valor;
        CaminoNodo(Nodo.padre, nel);
    }

    return Nodo.valor + camino;

}

function CostoNodo(Nodo, nel){

    if(Nodo.padre != null){
        suma = suma + Nodo.padre.valor;
        CostoNodo(Nodo.padre, nel);
    }

    return Nodo.valor + suma;

}
