class Arbol{
    constructor(){
        this.NodoPadre = this.CrearPadre();
    }

    CrearPadre(nombre = 36, valor = 36, padre = null, nivel = null){
        var nodo = new Nodo(nombre, valor, padre, nivel);
        return nodo;
    }
    
    CrearNodo(nombre, valor, padre, nivel){
        var nodo = new Nodo(nombre, valor, padre, nivel);
        return nodo;
    }
}
