var arbol = new Arbol();
var tp = [] , z , camino = '' , suma = 0;

//Parte izquierda
arbol.NodoPadre.Ni = arbol.CrearNodo(16, 16, arbol.NodoPadre, arbol.NodoPadre.nivel);

arbol.NodoPadre.Ni.Ni = arbol.CrearNodo(8, 8, arbol.NodoPadre.Ni, arbol.NodoPadre.Ni.nivel);

arbol.NodoPadre.Ni.Ni.Ni = arbol.CrearNodo('e', 4, arbol.NodoPadre.Ni.Ni, arbol.NodoPadre.Ni.Ni.nivel);
arbol.NodoPadre.Ni.Ni.Nd = arbol.CrearNodo(4, 4, arbol.NodoPadre.Ni.Ni, arbol.NodoPadre.Ni.Ni.nivel);

arbol.NodoPadre.Ni.Ni.Nd.Ni = arbol.CrearNodo('n', 2, arbol.NodoPadre.Ni.Ni.Nd, arbol.NodoPadre.Ni.Ni.Nd.nivel);
arbol.NodoPadre.Ni.Ni.Nd.Nd = arbol.CrearNodo(2, 2, arbol.NodoPadre.Ni.Ni.Nd, arbol.NodoPadre.Ni.Ni.Nd.nivel);

arbol.NodoPadre.Ni.Ni.Nd.Nd.Ni = arbol.CrearNodo('o', 1, arbol.NodoPadre.Ni.Ni.Nd.Nd, arbol.NodoPadre.Ni.Ni.Nd.Nd.nivel);
arbol.NodoPadre.Ni.Ni.Nd.Nd.Nd = arbol.CrearNodo('u', 1, arbol.NodoPadre.Ni.Ni.Nd.Nd, arbol.NodoPadre.Ni.Ni.Nd.Nd.nivel);

arbol.NodoPadre.Ni.Nd = arbol.CrearNodo(8, 8, arbol.NodoPadre.Ni, arbol.NodoPadre.Ni.nivel);

arbol.NodoPadre.Ni.Nd.Ni = arbol.CrearNodo('a', 4, arbol.NodoPadre.Ni.Nd, arbol.NodoPadre.Ni.Nd.nivel);
arbol.NodoPadre.Ni.Nd.Nd = arbol.CrearNodo(4, 4, arbol.NodoPadre.Ni.Nd, arbol.NodoPadre.Ni.Nd.nivel);

arbol.NodoPadre.Ni.Nd.Nd.Ni = arbol.CrearNodo('t', 2, arbol.NodoPadre.Ni.Nd.Nd, arbol.NodoPadre.Ni.Nd.Nd.nivel);
arbol.NodoPadre.Ni.Nd.Nd.Nd = arbol.CrearNodo('m', 2, arbol.NodoPadre.Ni.Nd.Nd, arbol.NodoPadre.Ni.Nd.Nd.nivel);

//Parte derecha
arbol.NodoPadre.Nd = arbol.CrearNodo(20, 20, arbol.NodoPadre, arbol.NodoPadre.nivel);

arbol.NodoPadre.Nd.Ni = arbol.CrearNodo(8, 8, arbol.NodoPadre.Nd, arbol.NodoPadre.Nd.nivel);

arbol.NodoPadre.Nd.Ni.Ni = arbol.CrearNodo(4, 4, arbol.NodoPadre.Nd.Ni, arbol.NodoPadre.Nd.Ni.nivel);

arbol.NodoPadre.Nd.Ni.Ni.Ni = arbol.CrearNodo('i', 2, arbol.NodoPadre.Nd.Ni.Ni, arbol.NodoPadre.Nd.Ni.Ni.nivel);
arbol.NodoPadre.Nd.Ni.Ni.Nd = arbol.CrearNodo(2, 2, arbol.NodoPadre.Nd.Ni.Ni, arbol.NodoPadre.Nd.Ni.Ni.nivel);

arbol.NodoPadre.Nd.Ni.Ni.Nd.Ni = arbol.CrearNodo('x', 1, arbol.NodoPadre.Nd.Ni.Ni.Nd, arbol.NodoPadre.Nd.Ni.Ni.Nd.nivel);
arbol.NodoPadre.Nd.Ni.Ni.Nd.Nd = arbol.CrearNodo('p', 1, arbol.NodoPadre.Nd.Ni.Ni.Nd, arbol.NodoPadre.Nd.Ni.Ni.Nd.nivel);

arbol.NodoPadre.Nd.Ni.Nd = arbol.CrearNodo(4, 4, arbol.NodoPadre.Nd.Ni, arbol.NodoPadre.Nd.Ni.nivel);

arbol.NodoPadre.Nd.Ni.Nd.Ni = arbol.CrearNodo('h', 2, arbol.NodoPadre.Nd.Ni.Nd, arbol.NodoPadre.Nd.Ni.Nd.nivel);
arbol.NodoPadre.Nd.Ni.Nd.Nd = arbol.CrearNodo('s', 2, arbol.NodoPadre.Nd.Ni.Nd, arbol.NodoPadre.Nd.Ni.Nd.nivel);

arbol.NodoPadre.Nd.Nd = arbol.CrearNodo(12, 12, arbol.NodoPadre.Nd, arbol.NodoPadre.Nd.nivel);

arbol.NodoPadre.Nd.Nd.Ni = arbol.CrearNodo(5, 5, arbol.NodoPadre.Nd.Nd, arbol.NodoPadre.Nd.Nd.nivel);

arbol.NodoPadre.Nd.Nd.Ni.Ni = arbol.CrearNodo(2, 2, arbol.NodoPadre.Nd.Nd.Ni, arbol.NodoPadre.Nd.Nd.Ni.nivel);

arbol.NodoPadre.Nd.Nd.Ni.Ni.Ni = arbol.CrearNodo('r', 1, arbol.NodoPadre.Nd.Nd.Ni.Ni, arbol.NodoPadre.Nd.Nd.Ni.Ni.nivel);
arbol.NodoPadre.Nd.Nd.Ni.Ni.Ni = arbol.CrearNodo('l', 1, arbol.NodoPadre.Nd.Nd.Ni.Ni, arbol.NodoPadre.Nd.Nd.Ni.Ni.nivel);

arbol.NodoPadre.Nd.Nd.Ni.Nd = arbol.CrearNodo('f', 3, arbol.NodoPadre.Nd.Nd.Ni, arbol.NodoPadre.Ni.nivel);

arbol.NodoPadre.Nd.Nd.Nd = arbol.CrearNodo(' ', 7, arbol.NodoPadre.Nd.Nd, arbol.NodoPadre.Nd.Nd.nivel);

//Imprime el arbol
console.log(arbol);

//Busqueda por nivel
var nivel = prompt("Ingrese un nivel:");
BusquedaNivel(arbol.NodoPadre, nivel);
console.log("Elementos en el nivel " + nivel + " : " + tp);

//Busqueda por elemento
var nel = prompt("Ingrese un valor a buscar:");
console.log("El elemento " + nel + " se encuentra en el nodo: ");
var nodox = BusquedaElemento(arbol.NodoPadre, nel);
console.log(nodox);

//Camino nodo
var cam = CaminoNodo(nodox, nel);
console.log(cam);

//Suma de elementos
var sum = CostoNodo(nodox, nel);
console.log(sum);

//Funciones

function BusquedaNivel(Nodo, nivel){
    
    if(Nodo.nivel == nivel)
        tp.push(Nodo.nombre);

    if(Nodo.hasOwnProperty('Ni'))
        BusquedaNivel(Nodo.Ni, nivel);
    
    if(Nodo.hasOwnProperty('Nd'))
        BusquedaNivel(Nodo.Nd, nivel);

}

function BusquedaElemento(Nodo, nel){

    if(Nodo.hasOwnProperty('Ni'))
        BusquedaElemento(Nodo.Ni, nel);
    
    if(Nodo.hasOwnProperty('Nd'))
        BusquedaElemento(Nodo.Nd, nel);

    if(Nodo.valor == nel){
        z = Nodo;
    }

    return z;
}

function CaminoNodo(Nodo, nel){

    if(Nodo.padre != null){
        camino = camino + ' ' + Nodo.padre.valor;
        CaminoNodo(Nodo.padre, nel);
    }

    return Nodo.valor + camino;

}

function CostoNodo(Nodo, nel){

    if(Nodo.padre != null){
        suma = suma + Nodo.padre.valor;
        CostoNodo(Nodo.padre, nel);
    }

    return Nodo.valor + suma;

}
