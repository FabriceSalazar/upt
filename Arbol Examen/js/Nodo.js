class Nodo{
    constructor(nombre, valor, padre, nivel){
        this.nombre = nombre;
        this.valor = valor;
        this.padre = padre;
        
        nivel == null ? this.nivel = 0 : this.nivel = nivel + 1;
    }
}
