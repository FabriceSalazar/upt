class Arbol {

    constructor() {
        this.nodoPadre = this.agregarNodoPadre();
        this.nivel = 0;
        this.buscarNodos = [];
    }

    agregarNodoPadre(){
        var nodo = new Nodo(null,null,"+", 566);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre,valor){
        var nodo = new Nodo(nodoPadre,posicion,nombre, valor);
        return nodo;
    }

    verificarNivelHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.buscarNodos.push(nodo.nombre);

        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);

        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodos;

    }

}
