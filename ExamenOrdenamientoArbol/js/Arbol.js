class Arbol{
    constructor(){
        this.NodoPadre = this.CrearPadre();
    }

    CrearPadre(padre, nivel, valor, posicion){
        var nodo = new Nodo(padre, nivel, valor, posicion);
        return nodo;
    }

    CrearDerecho(nodo, valor){
        if(valor <= nodo.valor){ 
            if(nodo.hasOwnProperty('Ni')){
                this.CrearIzquierdo(nodo.Ni, valor);
            }else{
                nodo.Ni = new Nodo(nodo, nodo.nivel, valor, "Izquierda");
            }
        }else{
            if(nodo.hasOwnProperty('Nd')){
                this.CrearDerecho(nodo.Nd, valor);
            }else{
                nodo.Nd = new Nodo(nodo, nodo.nivel, valor, "Derecha");
            }
        }
        
    }

    CrearIzquierdo(nodo, valor){
        if(valor <= nodo.valor){ 
            if(nodo.hasOwnProperty('Ni')){
                this.CrearIzquierdo(nodo.Ni, valor);
            }else{
                nodo.Ni = new Nodo(nodo, nodo.nivel, valor, "Izquierda");
            }
        }else{
            if(nodo.hasOwnProperty('Nd')){
                this.CrearDerecho(nodo.Nd, valor);
            }else{
                nodo.Nd = new Nodo(nodo, nodo.nivel, valor, "Derecha");
            }
        }
        
    }

    OrdenarNodos(nodo, valor){
        if(valor <= nodo.valor){
            this.CrearIzquierdo(nodo, valor);
        }else{
            this.CrearDerecho(nodo, valor);
        }
    }
    
}
