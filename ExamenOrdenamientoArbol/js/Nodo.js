class Nodo{
    constructor(padre, nivel, valor, posicion){
        this.padre = padre;
        this.valor = valor;
        this.posicion =  posicion;

        nivel == null ? this.nivel = 0 : this.nivel = nivel + 1;
    }
}