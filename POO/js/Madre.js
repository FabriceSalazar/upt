class Madre extends Abuela{
    constructor(){
        super();
        this.Conflexion = "Delgada";
        this.ApPaterno = "Campos";
        this.Nombre = "Rebeca";
    }

    get CareteristicasMadre(){
        return "Caracteriticas de la madre:" + " \n" + this.Conflexion + " \n" + this.ApPaterno + " \n"+ this.Nombre;
    }
}