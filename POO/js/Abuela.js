class Abuela{
    constructor(){
        this.Cabello = "Quebrado";
        this.ColorOjos = "Cafe claro";
        this.Piel = "Morena";
        this.Altura = 1.65;
    }

    get CareteristicasAbuela(){
        return "Caracteristicas de la abuela" + " \n" + this.Cabello + " \n" + this.ColorOjos + " \n" + this.Piel + 
        " \n" + this.Altura;
    }

}