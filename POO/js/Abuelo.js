class Abuelo{
    constructor(){
        this.Cabello = "Chino";
        this.ColorOjos = "Cafe claro";
        this.Piel = "Blanca";
        this.Altura = 1.80;
    }

    get CareteristicasAbuelo(){
        return "Caracteristicas del abuelo" + " \n" + this.Cabello + " \n" + this.ColorOjos + " \n" + this.Piel + 
        " \n" + this.Altura;
    }

}