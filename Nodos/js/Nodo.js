class Nodo{
    constructor(tipo, valor, nivel, padre, hijoI, hijoD){
        this.tipo = tipo;
        this.valor = valor;
        this.nivel = nivel;
        this.padre = padre;
        
        if(hijoI == 1)
            this.hijoI = this.CrearHijo(tipo = "i", valor = 0, nivel = this.nivel + 1, this.nivel);

        if(hijoD == 1)
            this.hijoD = this.CrearHijo(tipo = "d", valor = 0, nivel = this.nivel + 1, this.nivel);
    }

    CrearHijo(tipo, valor, nivel, padre){
        var CrearHijo = new Nodo(tipo = "i", valor = 0, nivel = this.nivel + 1, this.nivel);
        return CrearHijo;
    }
}